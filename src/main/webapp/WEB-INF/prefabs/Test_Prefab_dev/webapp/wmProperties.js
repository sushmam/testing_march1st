var _WM_APP_PROPERTIES = {
  "activeTheme" : "default",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "Test Prefab",
  "homePage" : "Main",
  "name" : "Test_Prefab",
  "platformType" : "DEFAULT",
  "securityEnabled" : "false",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "PREFAB",
  "version" : "1.0"
};